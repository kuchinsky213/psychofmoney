package com.ggs.psychologyofmoney.activ;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Fragment;
import androidx.room.Room;


import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ggs.psychologyofmoney.MainActivity;
import com.ggs.psychologyofmoney.R;
import com.ggs.psychologyofmoney.RoomNote.dao.DatabaseClient;
import com.ggs.psychologyofmoney.RoomNote.dao.Task;
import com.ggs.psychologyofmoney.ui.NotificationsFragment;


public class AddtoDiary extends AppCompatActivity implements NotificationsFragment.Callbacks {
    private EditText editTextTask, editTextDesc, editTextFinishBy;

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    NotificationsFragment fragment = new NotificationsFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addto_diary);
        editTextTask = findViewById(R.id.editTextTask);
        editTextDesc = findViewById(R.id.editTextDesc);
        editTextFinishBy = findViewById(R.id.editTextFinishBy);

        findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTask();
            }
        });
    }

    private void saveTask() {
        final String sTask = editTextTask.getText().toString().trim();
        final String sDesc = editTextDesc.getText().toString().trim();
        final String sFinishBy = editTextFinishBy.getText().toString().trim();

        if (sTask.isEmpty()) {
            editTextTask.setError("Task required");
            editTextTask.requestFocus();
            return;
        }

        if (sDesc.isEmpty()) {
            editTextDesc.setError("Desc required");
            editTextDesc.requestFocus();
            return;
        }

        if (sFinishBy.isEmpty()) {
            editTextFinishBy.setError("Finish by required");
            editTextFinishBy.requestFocus();
            return;
        }

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                //creating a task
                Task task = new Task();
                task.setTask(sTask);
                task.setDesc(sDesc);
                task.setFinishBy(sFinishBy);
                task.setFinished(false);

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//
//                Fragment fragment = new NotificationsFragment();
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();






//Tr
//                try {
//                    onMyContainerAttached();
//
////                fragmentTransaction.add(R.id.container, fragment);
////                fragmentTransaction.commit();
//                }
//                catch (Exception e){
//                    e.printStackTrace();
//                }








  startActivity(new Intent(getApplicationContext(), MainActivity.class));
               finish();
                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }


    @Override
    public void onMyContainerAttached() {


                fragmentTransaction.add(R.id.container, fragment);
                fragmentTransaction.commit();
    }
}